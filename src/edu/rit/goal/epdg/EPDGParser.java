package edu.rit.goal.epdg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import edu.rit.goal.epdg.object.Edge;
import edu.rit.goal.epdg.object.Vertex;
import edu.rit.goal.epdg.object.VertexType;

public class EPDGParser {
	private Java8BaseListener listener;
	private Map<String, DirectedGraph<Vertex, Edge>> graphs = new HashMap<>();
	private Map<String, Map<VertexType, List<Vertex>>> verticesByType = new HashMap<>();
	
	public void parseJavaClass(String body, int errorLineOffset) {
		Lexer lexer = new Java8Lexer(new ANTLRInputStream(body));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		
		listener = new Java8BaseListener(errorLineOffset);
		
		parser.removeErrorListeners();
		parser.addErrorListener(new ErrorListener(listener));

		ParserRuleContext t = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(listener, t);
		
		// Generate the graphs.
		for (String method : listener.getMethods()) {
			DirectedGraph<Vertex, Edge> graph = new DefaultDirectedGraph<>(Edge.class);
			graphs.put(method, graph);
			verticesByType.put(method, new HashMap<>());
			
			for (Vertex current : listener.getVertices(method)) {
				graph.addVertex(current);
				
				List<Vertex> list = verticesByType.get(method).get(current.getType());
				if (list == null)
					verticesByType.get(method).put(current.getType(), list = new ArrayList<>());
				list.add(current);
			}
			for (Edge current : listener.getEdges(method))
				graph.addEdge(current.getFromVertex(), current.getToVertex(), current);
		}
	}
	
	public Set<String> getMethods() {
		return listener.getMethods();
	}

	public Map<String, DirectedGraph<Vertex, Edge>> getGraphs() {
		return graphs;
	}

	public Map<String, Map<VertexType, List<Vertex>>> getVerticesByType() {
		return verticesByType;
	}
	
}