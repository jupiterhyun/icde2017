package edu.rit.goal.epdg.object;

import edu.rit.goal.epdg.object.Pattern.PatternType;

public class Variable {
	public static String substitute(String str, Solution sol, PatternType type, String pre, String post) {
		for (String uVar : sol.getVarMapping().keySet())
			str = str.replaceAll(((type != null) ? type + "\\." : "") + uVar, pre + sol.getVarMapping().get(uVar) + post);
		return str;
	}
}
