package edu.rit.goal.epdg.object;

import java.util.List;
import java.util.Map;

public class MatchingResult {
	private Map<Feedback, List<Feedback>> feedback;
	private List<Solution> solutions;
	public Map<Feedback, List<Feedback>> getFeedback() {
		return feedback;
	}
	public void setFeedback(Map<Feedback, List<Feedback>> feedback) {
		this.feedback = feedback;
	}
	public List<Solution> getSolutions() {
		return solutions;
	}
	public void setSolutions(List<Solution> solutions) {
		this.solutions = solutions;
	}
}
