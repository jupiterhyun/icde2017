package edu.rit.goal.epdg.object;

import edu.rit.goal.epdg.object.Pattern.PatternType;

public class PatternFeedback extends Feedback {
	private PatternType patternType;
	
	public PatternFeedback(FeedbackType type, String text, PatternType patternType) {
		super(type, text);
		this.patternType = patternType;
	}

	public PatternType getPatternType() {
		return patternType;
	}

}
