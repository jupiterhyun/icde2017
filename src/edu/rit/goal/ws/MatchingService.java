package edu.rit.goal.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Sets;

import edu.rit.goal.epdg.EPDGParser;
import edu.rit.goal.epdg.object.Constraint;
import edu.rit.goal.epdg.object.Feedback;
import edu.rit.goal.epdg.object.Feedback.FeedbackType;
import edu.rit.goal.epdg.object.MatchingResult;
import edu.rit.goal.epdg.object.Pattern;
import edu.rit.goal.epdg.object.Pattern.PatternType;
import edu.rit.goal.epdg.object.Solution;
import edu.rit.goal.epdg.object.VertexType;

public class MatchingService {
	
	public Map<String, Map<Feedback, List<Feedback>>> match(String submission, Map<String, String> patternsToApply, Map<String, String> constraintsToApply, int limit) {
		// Let's parse the submission.
		EPDGParser parser = new EPDGParser();
		
		// First, we need to wrap it with a class and a method.
		String wrapped = wrapSubmission(submission);
		
		List<String> errors = new ArrayList<>();
		Map<String, Map<Feedback, List<Feedback>>> feedback = new HashMap<>();
		Map<String, String> submissionMethodsMapping = new HashMap<>();
		
		// Parse the submission.
		try {
			parser.parseJavaClass(wrapped, 2);
		} catch (Exception oops) {
			errors.add("when parsing the submission");
		}
		
		// Parse the patterns.
		Map<String, Map<PatternType, Integer>> patterns = new HashMap<>();
		try {
			for (String method : patternsToApply.keySet()) {
				String[] selected = patternsToApply.get(method).split(",");
				
				Map<PatternType, Integer> typesAndTimes = new HashMap<>();
				patterns.put(method, typesAndTimes);
				
				for (String currentPattern : selected) {
					String[] typeAndNumber = currentPattern.split("--");
					
					PatternType type = PatternType.valueOf(typeAndNumber[0]);
					int times = Integer.valueOf(typeAndNumber[1]);
					
					typesAndTimes.put(type, times);
				}
			}
		} catch (Exception oops) {
			errors.add("Something went wrong when parsing the selected patterns.");
		}
		
		// Parse the constraints.
		Map<String, List<Constraint>> constraints = new HashMap<>();
		try {
			for (String method : constraintsToApply.keySet()) {
				String[] selected = constraintsToApply.get(method).split("\\|\\|\\|\\|\\|\\|");
				constraints.put(method, new ArrayList<>());
				
				for (String currentConstraint : selected) {
					Constraint c = Constraint.parse(currentConstraint);
					if (c != null)
						constraints.get(method).add(c);
				}
			}
		} catch (Exception oops) {
			errors.add("Something went wrong when parsing the selected constraints.");
		}
		
		if (errors.isEmpty()) {
			Set<String> submissionMethods = parser.getMethods();
			Set<String> expectedMethods = patternsToApply.keySet();
			
			matchSubmissions(0, new ArrayList<>(expectedMethods), submissionMethods, new HashMap<>(), patterns, constraints, parser, feedback, submissionMethodsMapping);
			
			// Change expected methods to the matched ones.
			Map<String, Map<Feedback, List<Feedback>>> tempFeedback = new HashMap<>();
			for (String expected : feedback.keySet())
				tempFeedback.put(submissionMethodsMapping.get(expected), feedback.get(expected));
			feedback = tempFeedback;
		} else
			for (String err : errors)
				System.err.println(err);
		
		return feedback;
	}
	
	private String wrapSubmission(String submission) {
		StringBuilder sb = new StringBuilder("public class Submission {\n");
		sb.append(submission);
		sb.append("}");
		
		return sb.toString();
	}
	
	private void matchSubmissions(int i, List<String> expectedMethods, Set<String> submissionMethods, Map<String, String> current, 
			Map<String, Map<PatternType, Integer>> patterns, Map<String, List<Constraint>> constraints, EPDGParser parser, 
			Map<String, Map<Feedback, List<Feedback>>> feedback, Map<String, String> mappingResult) {
		if (i == expectedMethods.size()) {
			Map<String, Map<Feedback, List<Feedback>>> currentFeedback = new HashMap<>();
			Map<String, Map<PatternType, List<Solution>>> currentSolutions = new HashMap<>();
			
			// Match patterns.
			for (String expected : current.keySet()) {
				String fromSubmission = current.get(expected);
				currentFeedback.put(expected, new HashMap<>());
				currentSolutions.put(expected, new HashMap<>());
				
				for (PatternType type : patterns.get(expected).keySet()) {
					int times = patterns.get(expected).get(type);
					
					MatchingResult result = Pattern.getInstance().getPattern(type).match(parser.getGraphs().get(fromSubmission), parser.getVerticesByType().get(fromSubmission), times);
					
					currentFeedback.get(expected).putAll(result.getFeedback());
					currentSolutions.get(expected).put(type, result.getSolutions());
				}
			}
			
			// Match constraints.
			for (String expected : current.keySet())
				if (constraints.get(expected) != null)
					for (Constraint c : constraints.get(expected)) {
						// Get all declarations.
						c.setParameters(parser.getVerticesByType().get(current.get(expected)).get(VertexType.DECL));
						c.setSolutions(currentSolutions.get(expected));
						c.setFunctionMapping(current);
						c.setGraph(parser.getGraphs().get(current.get(expected)));
						
						// f will be null when we are not able to check this constraint.
						for (Feedback f : c.match())
							currentFeedback.get(expected).put(f, new ArrayList<>());
					}
			
			// Compare feedback.
			double feedbackScore = computeScore(feedback);
			double currentFeedbackScore = computeScore(currentFeedback);
			
			if (feedback.isEmpty() || currentFeedbackScore > feedbackScore) {
				feedback.clear();
				feedback.putAll(currentFeedback);
				
				mappingResult.clear();
				mappingResult.putAll(current);
			}
		} else {
			String currentExpectedMethod = expectedMethods.get(i);
			// Remove those that are already mapped.
			for (String currentSubmissionMethod : Sets.difference(submissionMethods, new HashSet<>(current.values()))) {
				current.put(currentExpectedMethod, currentSubmissionMethod);
				matchSubmissions(i + 1, expectedMethods, submissionMethods, current, patterns, constraints, parser, feedback, mappingResult);
				current.remove(currentExpectedMethod);
			}
		}
	}
	
	private double computeScore(Map<String, Map<Feedback, List<Feedback>>> feedback) {
		double ret = 0.0;
		
		for (String f : feedback.keySet())
			for (Feedback feed : feedback.get(f).keySet()) {
				if (feed.getType().equals(FeedbackType.Correct))
					ret+=1.0;
				else if (feed.getType().equals(FeedbackType.Almost))
					ret+=0.5;
			}
		
		return ret;
	}
	
}